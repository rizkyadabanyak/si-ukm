<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('admin.layouts.components.style')

</head>
<body>
<div id="app">
    <div class="main-wrapper">
        @include('admin.layouts.partials.navbar')
        @include('admin.layouts.partials.sidebar')
        <div class="wrapper">
            @yield('content')
        </div>
        @include('admin.layouts.partials.footer')
    </div>
</div>
    @include('admin.layouts.components.script')

</body>
</html>
