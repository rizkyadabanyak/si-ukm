@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>UKM</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>
            @include('admin.layouts.partials.notice')

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Structure UKM</h4>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="{{route('admin.staff.store',$id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 ">Peridode </label>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Start Date</label>
                                        <div class="col-sm-6 col-md-7">
                                            <input type="date" name="start_date" class="form-control @error('title') is-invalid @enderror">
                                            @error('start_date')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">End Date</label>
                                        <div class="col-sm-6 col-md-7">
                                            <input type="date" name="end_date" class="form-control @error('title') is-invalid @enderror">
                                            @error('end_date')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Data Staff</label>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Role</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="role[]" class="form-control @error('title') is-invalid @enderror">
                                            @error('role')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name[]" class="form-control @error('title') is-invalid @enderror">
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="staff">

                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <a style="color: white" class="btn btn-success float-right addstaff">New Staff</a>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            @if($data->final == '0')
                                                <a href="{{route('admin.ukm.create')}}" style="color: white" class="btn btn-primary">Back</a>
                                            @endif
                                            <button class="btn btn-primary">Finish</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $('.addstaff').on('click',function (){
            addstaff();
        });
        function addstaff(){
            var staff = '<div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Data Staff</label> </div><div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Role</label> <div class="col-sm-12 col-md-7"> <input type="text" name="role[]" id="role" class="form-control @error('title') is-invalid @enderror"> @error('name') <div class="alert alert-danger">{{ $message }}</div> @enderror </div> </div> <div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label> <div class="col-sm-12 col-md-7"> <input type="text" name="name[]" id="name" class="form-control @error('title') is-invalid @enderror"> @error('name') <div class="alert alert-danger">{{ $message }}</div> @enderror </div> </div><div class="form-group row mb-4"> <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label> <div class="col-sm-12 col-md-7"> <a style="color: white" class="btn btn-danger float-right remove">Remove</a></div></div>';

            $('.staff').append(staff);
        };
        $('.remove').live('click', function(){
            $(this).parent().parent().parent().remove();
        });
    </script>
@endsection



