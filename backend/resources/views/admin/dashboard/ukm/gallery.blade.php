@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>UKM</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>

            @include('admin.layouts.partials.notice')

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Add galleries</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="{{route('admin.galleries.create',$data->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image[]" id="image" multiple="true">
                                        </div>

                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Galleries</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">img thumbnail</label>
                                        <div class="col-sm-12 col-md-7">

                                            <div class="row gutters-sm">
                                                @forelse($images as $img)
                                                    <div class="col-6 col-sm-4">
                                                        <label class="imagecheck mb-4">
                                                            <input name="images" id="thumbnail" type="checkbox" value="{{$img->id}}" class="imagecheck-input checkBoxClass" />
                                                            <figure class="imagecheck-figure">
                                                                <img src="{{asset($img->img)}}" alt="}" class="imagecheck-image">
                                                            </figure>
                                                        </label>
                                                    </div>
                                                    @empty
                                                        <a>u not have galleries</a>
                                                    @endforelse
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="checkbox" id="checkAll">check all</input>

                                    </div>
                                </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <a href="#" class="btn btn-danger" id="deleteRecord">delete</a>
                                        </div>
                                    </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>thumbnail</h4>

                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{route('admin.galleries.thumbnail')}}" method="post">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">img thumbnail</label>
                                        <div class="col-sm-12 col-md-7">

                                            <div class="row gutters-sm">
                                                @forelse($images as $img)
                                                    <div class="col-6 col-sm-4">
                                                        <label class="imagecheck mb-4">
                                                            @if($img->thumbnail == "1")
                                                                <input type="text" name="tmp" value="{{$img->id}}" hidden>
                                                                @endif
                                                            <input name="thumbnail" id="thumbnail" type="radio" value="{{$img->id}}" class="imagecheck-input" {{ $img->thumbnail == "1" ? 'checked' : '' }} />
                                                            <figure class="imagecheck-figure">
                                                                <img src="{{asset($img->img)}}" alt="}" class="imagecheck-image">
                                                            </figure>
                                                        </label>
                                                    </div>
                                                @empty
                                                    <a>u not have galleries</a>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">add</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $(function (e){
            $("#checkAll").click(function (){
                $(".checkBoxClass").prop('checked',$(this).prop('checked'));
            });
            $("#deleteRecord").click(function (e){
               e.preventDefault();
               var images = [];

               $("input:checkbox[name=images]:checked").each(function (){
                   images.push($(this).val());
               });
               $.ajax({
                   url : "{{route('admin.galleries.selectDelete')}}",
                   type:"DELETE",
                   data:{
                       _token:$("input[name=_token]").val(),
                       images:  images
                   },
                   success:function (response){
                       $.each(images,function (key,val){
                           $("#images"+val).remove();
                       })
                   }
               });
                window.location.href=window.location.href;
            });
        });
    </script>
@endsection
