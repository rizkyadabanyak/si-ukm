@extends('admin.layouts.app')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>UKM</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>Edit</h4>

                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="{{route('admin.ukm.update',$data->id)}}" method="post" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="name" id="name" class="form-control @error('title') is-invalid @enderror" value="{{($data != null) ? $data->name : '' }}">
                                            @error('name')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Category</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="form-control select2" name="category_id">
                                                @if($data != null)
                                                    @if($data->category_id != null)
                                                        <option value="{{$data->category_id}}">{{$data->categories->name}}</option>
                                                    @endif
                                                    @foreach($categories as $category)
                                                        @if($category->id != $data->category_id)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @error('category_id')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tags</label>
                                        <div class="col-sm-12 col-md-7">
                                            <select class="selectpicker form-control select2" name="tag[]" multiple data-live-search="'true">
                                                @if($data != null)
                                                    @foreach($tags as $tag)
                                                        <option value="{{$tag->id}}"
                                                                @foreach($data->tags as $myTag)
                                                                @if(in_array($tag->id,old('color',[$myTag->id]))) selected="selected"
                                                            @endif
                                                            @endforeach
                                                        >{{$tag->name}}</option>
                                                    @endforeach
                                                @else
                                                    @foreach($tags as $tag)
                                                        <option value="{{$tag->id}}" @if(in_array($tag->id,old('tag',[]))) selected="selected" @endif >{{$tag->name}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @error('tag')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Logo</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="file" name="image" id="image" value="{{($data != null) ? $data->logo : '' }}">
                                            <input name="newImage" value="{{($data != null) ? $data->logo : '' }}" hidden>
                                        </div>

                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Schedule</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="summernote-simple {{ $errors->has('schedule') ? 'is-invalid' :'' }}"
                                                      name="schedule" required> {{($data != null) ? $data->schedule : old('schedule') }} </textarea>
                                            @error('schedule')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Benefit</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="summernote-simple {{ $errors->has('benefit') ? 'is-invalid' :'' }}"
                                                      name="benefit" required>{{($data != null) ? $data->benefit : old('benefit') }}</textarea>
                                            @error('benefit')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Vision</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="summernote-simple {{ $errors->has('vision') ? 'is-invalid' :'' }}"
                                                      name="vision" required>{{($data != null) ? $data->vision : old('vision') }}</textarea>
                                            @error('vision')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Mission</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="summernote-simple {{ $errors->has('mission') ? 'is-invalid' :'' }}"
                                                      name="mission" required>{{($data != null) ? $data->mission : old('mission') }}</textarea>
                                            @error('dsc')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Description</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="summernote-simple {{ $errors->has('dsc') ? 'is-invalid' :'' }}"
                                                      name="dsc" required>{{($data != null) ? $data->dsc : old('dsc') }}</textarea>
                                            @error('dsc')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Next</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="card">
                            <div class="container">
                                <div class="d-flex justify-content-between pt-5">
                                    <div>
                                        <h4>staff</h4>
                                    </div>
                                    <a href="{{route('admin.staff.create',$data->id)}}" class="btn btn-success">Create</a>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data->staffs as $staff )
                                        <tr>
                                            <td>{{$staff->name}}</td>
                                            <td>{{$staff->role}}</td>
                                            <td>
                                                <a style="color: white" class="btn btn-info">Edit</a>
                                                <a href="{{route('admin.staff.destroy',[$data->id,$staff->id])}}" onclick="return confirm('Are you sure?')" style="color: white" class="btn btn-danger">Delete</a>

                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <script>

        $('#summernote').summernote({
            dialogsInBody: true,
            minHeight: 150,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['para', ['paragraph']]
            ]
        });
    </script>
@endsection
