@extends('admin.layouts.app')
@section('content')
    @include('admin.layouts.components.style-datatable')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>UKM</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Modules</a></div>
                    <div class="breadcrumb-item">DataTables</div>
                </div>
            </div>
            @include('admin.layouts.partials.notice')

            <div class="section-body">
                <h2 class="section-title">Categories</h2>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4><a href="{{route('admin.categories.create')}}" class="btn btn-primary">TAMBAH DATA</a> </h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="item">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>dsc</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#item').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('admin.categories.index')}}",
                },
                columns: [
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'dsc',
                        name: 'dsc'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false
                    },

                ]
            });
        });
    </script>
@endsection
