@extends('mahasiswa.layout')
@section('content')
    <div class="row d-flex justify-content-center   ">
        @foreach($data as $a)
                    @if($a->status == 'active')
                        <div class="col mt-5" >
                            <a href="{{route('show',[$a->id,$a->slug])}}">
                                <div class="card" style="width: 18rem;">
                                    @foreach($a->images as $image)
                                        @if($image->thumbnail == '1')
                                            <img class="card-img-top" src="{{asset($image->img)}}" height="200" alt="Card image cap">

                                        @endif

                                    @endforeach
                                    <div class="card-body">
                                        <h5 class="card-title">{{$a->name}}</h5>
                                        <p class="card-text">{!!  Illuminate\Support\Str::limit($a->dsc, 200 )  !!}</p>
                                        @foreach($a->tags as $tag)
                                            <span class="badge badge-pill badge-primary">{{$tag->name}}</span>

                                        @endforeach
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
        @endforeach


    </div>

@endsection
