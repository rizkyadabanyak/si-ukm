@extends('mahasiswa.layout')
@section('content')
    <div class="row d-flex justify-content-center">
        @foreach($data as $a)
            @foreach($recoms as $recom)
                @if($a->id == $recom->ukm_id && $a->status == 'active')
                    <div class="col mt-5">
                        <div class="card" style="width: 18rem;">
                            <img class="card-img-top" src="{{asset('stisla/assets/img/example-image.jpg')}}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{$a->name}}</h5>
                                <p class="card-text">{!!  Illuminate\Support\Str::limit($a->dsc, 200 )  !!}</p>
                                @foreach($a->tags as $tag)
                                    <span class="badge badge-pill badge-primary">{{$tag->name}}</span>

                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach

        @endforeach


    </div>

@endsection
