<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    @include('mahasiswa.components.style')

</head>
<body>
<div class="container">
    @include('mahasiswa.partials.navbar')

    @yield('content')

    @include('mahasiswa.partials.footer')

    @include('mahasiswa.components.script')

</div>


</body>
</html>
