<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffUkmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_ukms', function (Blueprint $table) {
            $table->foreignId('ukm_id')->constrained('ukms');
            $table->foreignId('staff_id')->constrained('staff');
            $table->foreignId('periode_id')->constrained('periodes');
            $table->primary(['ukm_id','staff_id']);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_ukms');
    }
}
