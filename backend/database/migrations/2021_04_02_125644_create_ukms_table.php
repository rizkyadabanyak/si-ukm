<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUkmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ukms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->constrained('categories');
            $table->foreignId('users_id')->constrained('users');
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('benefit');
            $table->string('mission');
            $table->string('vision');
            $table->string('schedule');
            $table->string('logo');
            $table->enum('final',[0,1])->default(0);

            $table->text('dsc');
            $table->enum('status',['active',['non-active']])->default('active');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ukms');
    }
}
