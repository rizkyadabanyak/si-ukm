<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUkmTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ukm_tags', function (Blueprint $table) {
            $table->foreignId('ukm_id')->constrained('ukms');
            $table->foreignId('tag_id')->constrained('tags');
            $table->timestamps();

            $table->primary(['ukm_id','tag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ukm_tags');
    }
}
