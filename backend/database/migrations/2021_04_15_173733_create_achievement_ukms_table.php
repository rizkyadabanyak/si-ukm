<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchievementUkmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievement_ukms', function (Blueprint $table) {

            $table->foreignId('ukm_id')->constrained('ukms');
            $table->foreignId('achievement_id')->constrained('achievements');

            $table->primary(['ukm_id','achievement_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievement_ukms');
    }
}
