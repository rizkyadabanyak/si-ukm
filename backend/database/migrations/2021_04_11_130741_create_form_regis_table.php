<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormRegisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_regis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ukm_id')->constrained('ukms');
            $table->string('name');
            $table->string('email');
            $table->string('class');
            $table->string('phone_number');
            $table->string('address');
            $table->string('major');
            $table->text('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_regis');
    }
}
