<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Admin',
           'role_id' => 1,
           'email' => 'riskipatra5@gmail.com',
            'password' => bcrypt('28112000'),
        ]);

        User::create([
            'name' => 'UKM Admin',
            'role_id' => 2,
            'email' => 'ukm@gmail.com',
            'password' => bcrypt('28112000'),
        ]);

    }
}
