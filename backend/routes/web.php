<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get('/',[\App\Http\Controllers\LandingController::class,'tags']);

Route::post('/select/tags',[\App\Http\Controllers\LandingController::class,'selectTags'])->name('selectTags');
Route::get('/recom',[\App\Http\Controllers\LandingController::class,'index'])->where('slug', '^[\w-]+$')->name('index');
Route::get('/',[\App\Http\Controllers\LandingController::class,'biasa'])->name('biasa');
Route::get('/detail/{id}/{slug}',[\App\Http\Controllers\LandingController::class,'show'])->name('show');

Auth::routes();

Route::group(['prefix'=>'admin/ganteng/','as'=>'admin.'],function (){
    Route::middleware([])->group(function () {
        Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        Route::resource('tags', \App\Http\Controllers\Admin\TagController::class);
        Route::get('tags/{id}/destroy', [\App\Http\Controllers\Admin\TagController::class,'destroy'])->name('tagsDestroy');

        Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
        Route::get('categories/{id}/destroy', [\App\Http\Controllers\Admin\CategoryController::class,'destroy'])->name('categoriesDestroy');

        Route::resource('ukm', \App\Http\Controllers\Admin\UkmController::class);
        Route::get('ukm/{id}/destroy', [\App\Http\Controllers\Admin\UkmController::class,'destroy'])->name('ukmDestroy');
//        Route::resource('staff', \App\Http\Controllers\Admin\StaffController::class);

        Route::get('staff/{id}', [\App\Http\Controllers\Admin\StaffController::class,'create'])->name('staff.create');
        Route::post('staff/{id}', [\App\Http\Controllers\Admin\StaffController::class,'store'])->name('staff.store');
        Route::get('staff/{idUKM}/{idStaff}', [\App\Http\Controllers\Admin\StaffController::class,'destroy'])->name('staff.destroy');


        Route::get('ukm/galleries/{id}', [\App\Http\Controllers\Admin\GalleryController::class,'index'])->name('galleries.index');
        Route::post('ukm/galleries/{id}/create', [\App\Http\Controllers\Admin\GalleryController::class,'create'])->name('galleries.create');
        Route::post('ukm/galleries/thumbnail', [\App\Http\Controllers\Admin\GalleryController::class,'image'])->name('galleries.thumbnail');
        Route::delete('ukm/galleries/selectDelete', [\App\Http\Controllers\Admin\GalleryController::class,'selectDelete'])->name('galleries.selectDelete');

    });
}
);
