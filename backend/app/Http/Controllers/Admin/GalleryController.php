<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Imageitem;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data = Ukm::find($id);
        $images = Image::whereUkmId($id)->get() ;

        view()->share([
            'data' => $data,
            'images' => $images
        ]);
        return view('admin.dashboard.ukm.gallery');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
//        dd($request->hasFile('image'));
        if ($request->hasFile('image'))
        {
            $files = $request->file('image');
            foreach ($files as $file){
                $data = new Image();

                $name = rand(99999,1);
                $extension = $file->getClientOriginalExtension();
                $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
                Storage::putFileAs('public',$file,$newName);

                $imgDB = 'storage/'.$newName;
                $data->ukm_id = $id;
                $data->img = $imgDB;
                $data->dsc = $imgDB;

                $data->save();

            }

        }
        return redirect()->back()->with('success','success upload image');
    }
    public function image(Request $request){
        $data = Image::find($request->thumbnail);

        if ($request->tmp != $request->thumbnail && $request->tmp != null){
            $tmpData = Image::find($request->tmp);
//            dd($tmpData);
            $tmpData->thumbnail = '0';

            $tmpData->save();
        }

        $data->thumbnail = '1';

//        dd($data);
        $data->save();

        return redirect()->back()->with('success','success set thumbnail');
    }
    public function selectDelete (Request $request){
        $data =$request->images;

        Image::whereIn('id',$data)->delete();

//        return response()->json(['success',"yey"]);
        return redirect()->back()->with('success','success delete images');

    }


}
