<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Tag;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            $data = Tag::all();

//        dd($data);
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<a href="tags/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.tagsDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.dashboard.tag.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.dashboard.tag.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Tag();

        $validates = [
            'name'  =>  'required|string',
            'dsc'   =>  'required'
        ];

        $request->validate($validates);

        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->dsc = $request->dsc;

        $data->save();

        return redirect()->route('admin.tags.index')->with('success','success add UKM');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $data = Tag::findOrFail($id);
        view()->share([
            'data' => $data
        ]);
        return view('admin.dashboard.tag.edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Tag::findOrFail($id);

        $validates = [
            'name'  =>  'required|string',
            'dsc'   =>  'required'
        ];

        $request->validate($validates);

        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->dsc = $request->dsc;

        $data->save();

        return redirect()->route('admin.tags.index')->with('success','edit add UKM');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        echo 'dqwdqw';
        $data = Tag::findOrFail($id);
        $data->delete();

        return redirect()->back()->with('danger','data has been deleted');
    }
}
