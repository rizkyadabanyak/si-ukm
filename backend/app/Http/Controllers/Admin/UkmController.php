<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use App\Models\Tag;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class UkmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    function check()
    {
        $userId = Auth::user()->id;
        $data = Ukm::whereUsersId($userId)->whereFinal('0')->first();

        return $data;
    }

    public function index(Request $request)
    {

        if($request->ajax()) {
//        $data = Item::orderBy('created_at','DESC')->get();
            if (Auth::user()->role_id == 2)
            {
                $data = Ukm::whereUsersId(Auth::user()->id)->whereFinal('1')->get();

            }else{
                $data = Ukm::whereFinal('1')->orderBy('updated_at','DESC')->get();

            }

//        dd($data);
            return DataTables::of($data)->addColumn('name', function($data){
                $a = '<a href="'.route('admin.galleries.index',$data->id).'">'.$data->name.'</a>';
                return $a;
                })
                ->addColumn('category_id', function($data){
                    $a = $data->categories->name;
                    return $a;
                })
                ->addColumn('action', function($data){
                    $button = '<a href="ukm/'.$data->id.'/edit'.'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>';
                    $button .= '<a  href="'.route('admin.ukmDestroy',$data->id).'" class="btn btn-danger" onclick="return confirm('.'`Are you sure?`'.')"><i class="fa fa-trash"></i> Delete</a>';
                    return $button;
                })
                ->rawColumns(['name','category_id','action'])
                ->make(true);
        }
        return view('admin.dashboard.ukm.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        $categories = Category::all();
        $data = $this->check();

        view()->share([
            'tags' => $tags,
            'categories' => $categories,
            'data' => $data
        ]);
        return view('admin.dashboard.ukm.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->check();

        $validates = [
            'name'      => 'required',
            'category_id'=> 'required',
            'tag'=> 'required',
            'schedule'=> 'required',
            'benefit'=> 'required',
            'vision'=> 'required',
            'dsc'       => 'required',

        ];
        $request->validate($validates);

        if ($data == null ){
            $data = new Ukm();
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
            Storage::putFileAs('public',$file,$newName);

            $imgDB = 'storage/'.$newName;
            $data->logo = $imgDB;
        }else{
            $data->logo = $request->newImage;
        }

        $data->category_id = $request->category_id;
        $data->users_id = Auth::user()->id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->schedule = $request->schedule;
        $data->benefit = $request->benefit;
        $data->vision = $request->vision;
        $data->mission = $request->mission;
        $data->dsc = $request->dsc;
        $data->final = '0';



        $data->save();

        $tags = $request->tag;
        $data->tags()->sync($tags);

        return redirect()->route('admin.staff.create',$data->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $data = Ukm::findOrFail($id);
        $categories = Category::all();
        $tags = Tag::all();


        view()->share([
            'data' => $data,
            'categories' => $categories,
            'tags' => $tags,
        ]);
        return view('admin.dashboard.ukm.edit');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Ukm::findOrFail($id);

        $validates = [
            'name'      => 'required',
            'category_id'=> 'required',
            'tag'=> 'required',
            'schedule'=> 'required',
            'benefit'=> 'required',
            'vision'=> 'required',
            'dsc'       => 'required',

        ];
        $request->validate($validates);

        if ($data == null ){
            $file = $request->file('image');

            $name = rand(999999,1);
            $extension = $file->getClientOriginalExtension();
            $newName = $name.'.'.$extension;
//                $path = $request->file('image')->storeAs('public',$newName);
            Storage::putFileAs('public',$file,$newName);

            $imgDB = 'storage/'.$newName;
            $data->logo = $imgDB;
        }else{
            $data->logo = $request->newImage;
        }


        $data->category_id = $request->category_id;
        $data->users_id = Auth::user()->id;
        $data->name = $request->name;
        $data->slug = \Str::slug($request->name);
        $data->schedule = $request->schedule;
        $data->benefit = $request->benefit;
        $data->vision = $request->vision;
        $data->mission = $request->mission;
        $data->dsc = $request->dsc;
        $data->final = '1';



        $data->save();

        $tags = $request->tag;
        $data->tags()->sync($tags);


        return redirect()->route('admin.ukm.index')->with('success','success add UKM');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data = Ukm::findOrFail($id);
            $data->delete();
            return redirect()->route('admin.ukm.index')->with('danger','delete UKM success');

        }catch (\Exception $e){
//            return $e->getMessage();
            return redirect()->back()->with('warning','cant delete this data bcs  : ' . $e->getMessage());
        }


    }
}
