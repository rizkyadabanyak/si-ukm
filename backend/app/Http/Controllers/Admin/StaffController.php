<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Periode;
use App\Models\Staff;
use App\Models\StaffUkm;
use App\Models\Ukm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = Ukm::find($id);

        view()->share([
           'id' => $id,
            'data' => $data
        ]);

        return view('admin.dashboard.ukm.staff.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        $dataCount = count($request->role);

//        dd($request->name);
        $validates = [
            'start_date' => 'required',
            'end_date' => 'required'
        ];

        $request->validate($validates);

        $periode = new Periode();
        $periode->start_date = $request->start_date;
        $periode->end_date = $request->end_date;
        $periode->save();

        for ($i =0 ; $i<$dataCount;$i++){

            if ($request->role[$i] == null || $request->name[$i] == null){
                return redirect()->back()->with('warning','there is an empty field');
            }

            $data = new Staff();
            $data->role = $request->role[$i];
            $data->name = $request->name[$i];
            $data->save();

            $staffUkm = new StaffUkm();
            $staffUkm->ukm_id = $id;
            $staffUkm->staff_id = $data->id;
            $staffUkm->periode_id = $periode->id;

            $staffUkm->save();
        }
        $ukm = Ukm::find($id);
        $ukm->final = '1';

        $ukm->save();

        return redirect()->route('admin.ukm.index')->with('success','success add UKM');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idUKM,$idStaff)
    {
        $data = StaffUkm::where([
            'ukm_id' => $idUKM,
            'staff_id' => $idStaff
        ])->delete();

        $staff = Staff::find($idStaff);
        $staff->delete();

        return redirect()->back();

    }
}
