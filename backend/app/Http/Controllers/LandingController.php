<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Tag;
use App\Models\Ukm;
use App\Models\UkmTag;
use Illuminate\Http\Request;

class LandingController extends Controller

{

    public function tags(){
        $selects = \Session::get('select');

        $data = Tag::all();

        if ($selects != null){
            return redirect()->route('index');
        }
        else{
        }

        view()->share([
            'datas' => $data
        ]);
        return view('mahasiswa.content.landing');
    }

    public function selectTags (Request $request){
        $tags = $request->tag;
        $select = [];
        if ($tags != null){
            $i = 0;
            $array = -1;
//        dd($tags);
            foreach ($tags as $tag){
                $i++;
                $array++;
                $select[$array] = [
                    'tag_id' => $tag
                ];
            }
            \Session::put('select',$select);
            return redirect()->route('index');
        }
        return redirect()->route('index');
    }
    public function index(){
        $selects = \Session::get('select');
        $data = Ukm::all();

        if ($selects != null ){
            $posts = \DB::table('ukm_tags');
            foreach ($selects as $select) {
                $posts = $posts->orwhere('tag_id', $select['tag_id']);
            }
            $fix = $posts->distinct()->get(['ukm_id']);;
            view()->share([
                'recoms' => $fix,
                'data' => $data
            ]);
            return view('mahasiswa.content.recom');
        }
        return redirect()->route('biasa');
    }
    public function biasa(){
        $data = Ukm::all();
        view()->share([
            'data' => $data
        ]);
        return view('mahasiswa.content.show');
    }

    public function show($id,$slug){
        $data = Ukm::findOrFail($id);

        if ($slug != $data->slug){
            return abort(404);

        }

        view()->share([
            'data' => $data
        ]);
        return view('mahasiswa.content.detail');
    }
}
