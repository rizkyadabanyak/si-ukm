<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    public function ukm(){
        return $this->belongsToMany(Staff::class,'staff_ukms');
    }
}
