<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ukm extends Model
{
    use HasFactory;


    public function tags(){
        return $this->belongsToMany(Tag::class,'ukm_tags','ukm_id','tag_id');
    }

    public function ukmTags(){
        return $this->hasMany(UkmTag::class);
    }

    public function categories(){
        return $this->belongsTo(Category::class,'category_id');

    }

    public function images(){
        return $this->hasMany(Image::class);
    }


    public function staffs(){
        return $this->belongsToMany(Staff::class,'staff_ukms','ukm_id','staff_id');
    }
}
