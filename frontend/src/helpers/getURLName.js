const getURLName = (namaUKM) => {
    let urlUKM = namaUKM.replaceAll('-', '');
    urlUKM = urlUKM.replace(/\s\s+/g, ' ');
    urlUKM = urlUKM.replaceAll(' ', '-');

    return urlUKM;
}

export default getURLName;