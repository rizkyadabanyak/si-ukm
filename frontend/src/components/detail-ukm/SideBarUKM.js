import '../../css/detail-ukm/SideBarUKM.css';

import { Form } from 'react-bootstrap';
import { useState } from 'react';

const SideBarUKM = ({ ukm }) => {
    const [checkedSection, setCheckedSection] = useState('deskripsi');

    return (<>
        <Form className="ukm-sidebar-layout">
            {['radio'].map((type) => (
                <div key={`default-${type}`} className="mb-3 d-flex flex-column align-items-end justify-content-center">
                    <div className="ukm-sidebar-link form-check">
                        <label title="" for={`default-0`} className="form-check-label" >
                            DESKRIPSI
                        </label>
                        <input 
                            name="sidebar-ukm" type="radio" id={`default-0`} value="deskripsi" className="form-check-input" defaultChecked="true"/>
                    </div>
                    { (ukm.galleries).length > 0 &&
                        <div className="ukm-sidebar-link form-check">
                            <label title="" for={`default-2`} className="form-check-label" >GALERI</label>
                            <input name="sidebar-ukm" type="radio" id={`default-2`} value="galeri" className="form-check-input"/>
                        </div>
                    }

                    <div className="ukm-sidebar-link form-check">
                        <label title="" for={`default-3`} className="form-check-label" >DETAIL</label>
                        <input name="sidebar-ukm" type="radio" id={`default-3`} value="visiMisi" className="form-check-input"/>
                    </div>
                    
                    { ukm.achievements.length > 0 &&
                        <div className="ukm-sidebar-link form-check">
                            <label title="" for={`default-7`} className="form-check-label">ACHIEVEMENTS</label>
                            <input name="sidebar-ukm" type="radio" id={`default-7`} value="achievements" className="form-check-input" />
                        </div>
                    }

                    <div className="ukm-sidebar-link form-check">
                        <label title="" for={`default-8`} className="form-check-label">PERTANYAAN</label>
                        <input name="sidebar-ukm" type="radio" id={`default-8`} className="form-check-input" />
                    </div>

                    
                </div>
            ))}
        </Form>
    </>);
}

export default SideBarUKM;