import { rest } from 'msw';
import { setupServer } from 'msw/node';
import '@testing-library/jest-dom';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { server as apiServer } from '../../helpers/server';
import PertanyaanUKM from './PertanyaanUKM';

const server = setupServer(
    rest.post(`${apiServer}api/v1/ukm/questions/:id`, (req, res, ctx) => {
        return res(ctx.json({ type: 'success' }))
    }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('Successfully submited question form should show a success alert', async () => {
    render(
        <MemoryRouter>
            <PertanyaanUKM ukmId="1" />
        </MemoryRouter>
    );

    const nrpInput = screen.getByLabelText('NRP');
    const nameInput = screen.getByLabelText('Nama');
    const emailInput = screen.getByLabelText('Email');
    const whatsAppInput = screen.getByLabelText('No Whatsapp');
    const questionInput = screen.getByLabelText('Pertanyaan');

    fireEvent.change(nrpInput, { target: { value: '2103191011' } });
    fireEvent.change(nameInput, { target: { value: 'Brillianta Bintang Virgantara' } });
    fireEvent.change(emailInput, { target: { value: 'brilliantabintangv@gmail.com' } });
    fireEvent.change(whatsAppInput, { target: { value: '085546697238' } });
    fireEvent.change(questionInput, { target: { value: 'Apa itu ?' } });

    fireEvent.click(screen.getByText('Tanyakan'));

    await waitFor(() => screen.getByText('Pertanyaan Berhasil di tambahkan'));

    expect(screen.getByText('Pertanyaan Berhasil di tambahkan')).toBeDefined();
});

test('Input should be cleared after successful input', async () => {
    render(
        <MemoryRouter>
            <PertanyaanUKM ukmId="1" />
        </MemoryRouter>
    );

    const nrpInput = screen.getByLabelText('NRP');
    const nameInput = screen.getByLabelText('Nama');
    const emailInput = screen.getByLabelText('Email');
    const whatsAppInput = screen.getByLabelText('No Whatsapp');
    const questionInput = screen.getByLabelText('Pertanyaan');

    fireEvent.change(nrpInput, { target: { value: '2103191011' } });
    fireEvent.change(nameInput, { target: { value: 'Brillianta Bintang Virgantara' } });
    fireEvent.change(emailInput, { target: { value: 'brilliantabintangv@gmail.com' } });
    fireEvent.change(whatsAppInput, { target: { value: '085546697238' } });
    fireEvent.change(questionInput, { target: { value: 'Apa itu ?' } });

    fireEvent.click(screen.getByText('Tanyakan'));

    await waitFor(() => screen.getByText('Pertanyaan Berhasil di tambahkan'));

    await waitFor(() => {
        expect(nrpInput).toHaveDisplayValue("");
        expect(nameInput).toHaveDisplayValue("");
        expect(emailInput).toHaveDisplayValue("");
        expect(whatsAppInput).toHaveDisplayValue("");
        expect(questionInput).toHaveDisplayValue("");
    });
});