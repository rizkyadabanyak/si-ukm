import {rest} from 'msw';
import {setupServer} from 'msw/node';
import '@testing-library/jest-dom';
import {render, fireEvent, waitFor, screen} from '@testing-library/react';
import FormPendaftaran from './FormPendaftaran';
import {Route, MemoryRouter} from 'react-router-dom';
import { server as apiServer } from '../../helpers/server';

const server = setupServer(
    rest.post(`${apiServer}api/v1/ukm/regis/:id`, (req, res, ctx) => {
      return res(ctx.json({type: 'success'}))
    }),
  )
  
beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('Successfully submited registration form should show a success alert', async () => {
    render(
      <MemoryRouter initialEntries={['/ukm/1/ukm-a/daftar']}>
        <Route exact path="/ukm/:id/:name/daftar">
          <FormPendaftaran />
        </Route>
      </MemoryRouter>
    );

    const nrpInput = screen.getByLabelText('NRP');
    const emailInput = screen.getByLabelText('Email');
    const nameInput = screen.getByLabelText('Nama Lengkap');
    const classInput = screen.getByLabelText('Kelas');
    const majorInput = screen.getByLabelText('Jurusan');
    const addressInput = screen.getByLabelText('Alamat');
    const numberInput = screen.getByLabelText('Nomor HP');
    const reasonInput = screen.getByLabelText('Alasan Mengikuti UKM');

    fireEvent.change(nrpInput, {target: {value: '2103191011'}});
    fireEvent.change(emailInput, {target: {value: 'brilliantabintangv@gmail.com'}});
    fireEvent.change(nameInput, {target: {value: 'Brillianta Bintang Virgantara'}});
    fireEvent.change(classInput, {target: {value: '2 D3 IT A'}});
    fireEvent.change(majorInput, {target: {value: 'Teknik Informatika'}});
    fireEvent.change(addressInput, {target: {value: 'Griya Intan Permai'}});
    fireEvent.change(numberInput, {target: {value: '085546697238'}});
    fireEvent.change(reasonInput, {target: {value: 'Bagus'}});

    fireEvent.click(screen.getByText('DAFTAR'));

    await waitFor(() => screen.getByText('Pendaftaran Berhasil'));

    expect(screen.getByText('Pendaftaran Berhasil')).toBeDefined();
});

test('Input should be cleared after successful input', async () => {
  render(
    <MemoryRouter initialEntries={['/ukm/1/ukm-a/daftar']}>
      <Route exact path="/ukm/:id/:name/daftar">
        <FormPendaftaran />
      </Route>
    </MemoryRouter>
  );

  const nrpInput = screen.getByLabelText('NRP');
  const emailInput = screen.getByLabelText('Email');
  const nameInput = screen.getByLabelText('Nama Lengkap');
  const classInput = screen.getByLabelText('Kelas');
  const majorInput = screen.getByLabelText('Jurusan');
  const addressInput = screen.getByLabelText('Alamat');
  const numberInput = screen.getByLabelText('Nomor HP');
  const reasonInput = screen.getByLabelText('Alasan Mengikuti UKM');

  fireEvent.change(nrpInput, {target: {value: '2103191011'}});
  fireEvent.change(emailInput, {target: {value: 'brilliantabintangv@gmail.com'}});
  fireEvent.change(nameInput, {target: {value: 'Brillianta Bintang Virgantara'}});
  fireEvent.change(classInput, {target: {value: '2 D3 IT A'}});
  fireEvent.change(majorInput, {target: {value: 'Teknik Informatika'}});
  fireEvent.change(addressInput, {target: {value: 'Griya Intan Permai'}});
  fireEvent.change(numberInput, {target: {value: '085546697238'}});
  fireEvent.change(reasonInput, {target: {value: 'Bagus'}});

  fireEvent.click(screen.getByText('DAFTAR'));

  await waitFor(() => screen.getByText('Pendaftaran Berhasil'));
  
  await waitFor(() => {
    expect(nrpInput).toHaveDisplayValue("");
    expect(emailInput).toHaveDisplayValue("");
    expect(nameInput).toHaveDisplayValue("");
    expect(classInput).toHaveDisplayValue("");
    expect(nrpInput).toHaveDisplayValue("");
    expect(majorInput).toHaveDisplayValue("");
    expect(addressInput).toHaveDisplayValue("");
    expect(numberInput).toHaveDisplayValue("");
    expect(reasonInput).toHaveDisplayValue("");
  });
});