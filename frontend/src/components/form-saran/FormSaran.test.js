import { rest } from 'msw';
import { setupServer } from 'msw/node';
import '@testing-library/jest-dom';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { server as apiServer } from '../../helpers/server';
import FormSaran from './FormSaran';

const server = setupServer(
    rest.post(`${apiServer}api/v1/suggestions`, (req, res, ctx) => {
        return res(ctx.json({ type: 'success' }))
    }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('Successfully submited suggestion form should show a success alert', async () => {
    render(
        <MemoryRouter>
            <FormSaran />
        </MemoryRouter>
    );

    const nrpInput = screen.getByLabelText('NRP');
    const nameInput = screen.getByLabelText('Nama');
    const majorInput = screen.getByLabelText('Jurusan');
    const reasonInput = screen.getByLabelText('Alasan Mengajukan UKM');
    const nameSuggestionInput = screen.getByLabelText('Saran Nama UKM');

    fireEvent.change(nrpInput, { target: { value: '2103191011' } });
    fireEvent.change(nameInput, { target: { value: 'Brillianta Bintang Virgantara' } });
    fireEvent.change(majorInput, { target: { value: 'Teknik Informatika' } });
    fireEvent.change(reasonInput, { target: { value: 'Bagus' } });
    fireEvent.change(nameSuggestionInput, { target: { value: 'ukm A' } });

    fireEvent.click(screen.getByText('KIRIM'));

    await waitFor(() => screen.getByText('Saran nama ukm berhasil diajukan!'));

    expect(screen.getByText('Saran nama ukm berhasil diajukan!')).toBeDefined();
});

test('Input should be cleared after successful input', async () => {
    render(
        <MemoryRouter>
            <FormSaran />
        </MemoryRouter>
    );

    const nrpInput = screen.getByLabelText('NRP');
    const nameInput = screen.getByLabelText('Nama');
    const majorInput = screen.getByLabelText('Jurusan');
    const reasonInput = screen.getByLabelText('Alasan Mengajukan UKM');
    const nameSuggestionInput = screen.getByLabelText('Saran Nama UKM');

    fireEvent.change(nrpInput, { target: { value: '2103191011' } });
    fireEvent.change(nameInput, { target: { value: 'Brillianta Bintang Virgantara' } });
    fireEvent.change(majorInput, { target: { value: 'Teknik Informatika' } });
    fireEvent.change(reasonInput, { target: { value: 'Bagus' } });
    fireEvent.change(nameSuggestionInput, { target: { value: 'ukm A' } });

    fireEvent.click(screen.getByText('KIRIM'));

    await waitFor(() => screen.getByText('Saran nama ukm berhasil diajukan!'));

    await waitFor(() => {
        expect(nrpInput).toHaveDisplayValue("");
        expect(nameInput).toHaveDisplayValue("");
        expect(nrpInput).toHaveDisplayValue("");
        expect(majorInput).toHaveDisplayValue("");
        expect(nameSuggestionInput).toHaveDisplayValue("");
        expect(reasonInput).toHaveDisplayValue("");
    });
});