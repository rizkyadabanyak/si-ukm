import '../../css/kategori-ukm/KategoriUKM.css';
import { useEffect, useState } from 'react';

const Nalar = () => {
  const imagesPath = '/images/kategori-ukm/';

  const [kategoriUKM, setKategoriUKM] = useState([]);

  useEffect(() => {
      fetch(`http://103.30.145.87/api/v1/ukm/categories/penalaran?token=${process.env.REACT_APP_API_TOKEN}`)
      .then(res => res.json())
      .then(resJson => setKategoriUKM(resJson.data)); 
  });

  return (<>

<div>

  <div>
    <span className="d-block kategori-block">
      <p>K A T E G O R I &nbsp; P E N A L A R A N </p>
    </span>
  </div>

  <div className="container-banyak ">
      <div className="card nalar-card">
        <img src={`${imagesPath}robot.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card nalar-card">
        <img src={`${imagesPath}mhe.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card nalar-card">
        <img src={`${imagesPath}mahetala.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card nalar-card">
        <img src={`${imagesPath}ent.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card nalar-card">
        <img src={`${imagesPath}e2c.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card nalar-card">
        <img src={`${imagesPath}dirgantara.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a>
      </div>


  </div>
</div>


  

</> );
}

export default Nalar;