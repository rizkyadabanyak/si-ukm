import '../../css/kategori-ukm/KategoriUKM.css';
import { useEffect, useState } from 'react';

const Olahraga = () => {
  const imagesPath = '/images/kategori-ukm/';

  const [kategoriUKM, setKategoriUKM] = useState([]);

  useEffect(() => {
      fetch(`http://103.30.145.87/api/v1/ukm/categories?token=${process.env.REACT_APP_API_TOKEN}`)
      .then(res => res.json())
      .then(resJson => setKategoriUKM(resJson.data)); 
  });

  return (<>

<div>
  <div>
    <span className="d-block kategori-block">
      <p>K A T E G O R I &nbsp; O L A H R A G A </p>
    </span>
  </div>

  <div className="container-banyak ">
      <div className="card olahraga-card">
          <img src={`${imagesPath}futsal.png`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card olahraga-card">
          <img src={`${imagesPath}basket.png`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card olahraga-card">
          <img src={`${imagesPath}badminton.png`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card olahraga-card">
          <img src={`${imagesPath}tenis.png`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card olahraga-card">
          <img src={`${imagesPath}voli.png`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card olahraga-card">
          <img src={`${imagesPath}karate.png`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card olahraga-card">
          <img src={`${imagesPath}pencaksilat.jpg`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card olahraga-card">
          <img src={`${imagesPath}taekwondo.jpeg`} className="card-img-top" alt="icon" />
          <a href="/ukm/paduan-suara"><div className="card-footer">READ MORE</div></a> 
      </div>
  </div>
</div>

</> );
}

export default Olahraga;