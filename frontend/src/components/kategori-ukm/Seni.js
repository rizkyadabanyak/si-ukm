import '../../css/kategori-ukm/KategoriUKM.css';
import { useEffect, useState } from 'react';

const Seni = () => {
  const imagesPath = '/images/kategori-ukm/';

  const [kategoriUKM, setKategoriUKM] = useState([]);

  useEffect(() => {
      fetch(`http://103.30.145.87/api/v1/ukm/categories?token=${process.env.REACT_APP_API_TOKEN}`)
      .then(res => res.json())
      .then(resJson => setKategoriUKM(resJson.data)); 
  });

  return (<>

  <div>

  <div>
      <span className="d-block kategori-block">
        <p>K A T E G O R I &nbsp; K E S E N I A N </p>
      </span>
  </div>

  <div className="container-banyak ">
      <div className="card seni-card">
        <img src={`${imagesPath}usi.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card seni-card">
        <img src={`${imagesPath}psm.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card seni-card">
        <img src={`${imagesPath}tari.jpg`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card seni-card">
        <img src={`${imagesPath}musik.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card seni-card">
        <img src={`${imagesPath}cinema.png`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>

      <div className="card seni-card">
        <img src={`${imagesPath}frens.jpg`} className="card-img-top" alt="icon" />
        <a href="#"><div className="card-footer">READ MORE</div></a> 
      </div>
  </div>
</div>

</> );
}

export default Seni;