import React from 'react';
//Include Sweetalert
import Swal from 'sweetalert2'

class Alert extends React.Component {
  //Button Click Function
  opensweetalert()
  {
    Swal.fire({
      title: 'Therichpost',
      text: "Hello",
      type: 'success',
      
    })
  }
  //Button Click Function

  opensweetalertdanger()
  {
    Swal.fire({
      title: 'Therichpost',
      text: "OOPS",
      type: 'warning',
      
      
    })
  }
  render() {
   
    return (
<div className="maincontainer">
  
  <h1>Reactjs</h1>
  
  <button onClick={this.opensweetalert}>Open Success Sweetalert Popup</button>
  <button onClick={this.opensweetalertdanger}>Open Danger Sweetalert Popup</button>
</div>
)
};
}

export default Alert;