import '@testing-library/jest-dom';
import NavigationBar from './NavigationBar';
import React from 'react';
import { createMemoryHistory } from 'history';
import { render, fireEvent } from "@testing-library/react";
import { Router } from 'react-router-dom';
import { MemoryRouter } from 'react-router-dom';

/* test('It should display UKMs that contains keyword from search input', () => {
    const history = createMemoryHistory();
    const setSearch = jest.fn((value) => {})
    render(
        <Router history={history}>
            <NavigationBar setSearch={setSearch}/>
        </Router>
    );

    const searchInput = queryByPlaceholderText('Cari UKM');
      
    fireEvent.change(searchInput, {target: {value: 'Bad'}});

    expect(searchInput.value).toBe('Badminton');

});*/


//Jadi barusan aku nyoba ini, buat memastikan apakah filenya bisa ke render
//atau ngga tapi ternyata gabisa, nanti coba run en bin
it("render correctly", () => {
    const { queryByPlaceholderText } = render(
        <MemoryRouter>
            <NavigationBar />
        </MemoryRouter>
    )

    expect(queryByPlaceholderText("Cari UKM")).toBeTruthy()
})