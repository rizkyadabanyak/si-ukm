import '@testing-library/jest-dom';
import { render, screen } from "@testing-library/react";
import { Router } from 'react-router-dom';
import UKMHasilRekomendasi from './UKMHasilRekomendasi';
import { createMemoryHistory } from 'history';
import { server } from '../../helpers/server';
import slugify from 'slugify';

const recommendedUKM = {
    data: {
        '1': {
            id: 1,
            name: 'Volley',
            logo: 'img/volley-logo.jpg' 
        },
        '2': {
            id: 2,
            name: 'Basket',
            logo: 'img/basket-logo.jpg'  
        }
    },
    recoms: [
        {
            ukm_id: 1
        }, 
        {
            ukm_id: 2
        }
    ]
};

test('It should have the correct amount of recommended UKM', async () => {
    const history = createMemoryHistory();
    render(
        <Router history={history}>
            <UKMHasilRekomendasi recommendedUKM={recommendedUKM}/>
        </Router>
    );

    expect(await screen.findAllByRole("recommendedUKM")).toHaveLength(2);
});

test('It should display the correct logo for the corresponding UKM', () => {
    const history = createMemoryHistory();
    render(
        <Router history={history}>
            <UKMHasilRekomendasi recommendedUKM={recommendedUKM}/>
        </Router>
    );
    
    const ukmVolleyImage = screen.getAllByRole("recommendedUKMImage")[0];
    expect(ukmVolleyImage.src).toBe(`${server}img/volley-logo.jpg`);
});

test('It should have hyperlinks to go to the correct ukm detail page', async () => {
    const history = createMemoryHistory();
    render(
        <Router history={history}>
            <UKMHasilRekomendasi recommendedUKM={recommendedUKM}/>
        </Router>
    );
    
    const ukmVolleyLink = screen.getAllByRole("recommendedUKMLink")[0];

    expect(ukmVolleyLink.href).toContain(`/ukm/1/${slugify('Volley')}`);
});