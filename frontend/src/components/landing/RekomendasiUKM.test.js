import '@testing-library/jest-dom';
import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { MemoryRouter } from 'react-router-dom';
import { server as apiServer } from '../../helpers/server';
import RekomendasiUKM from './RekomendasiUKM';

const server = setupServer(
    rest.get(`${apiServer}api/v1/tag`, (req, res, ctx) => {
        return res(ctx.json({ 
            tags: [
                { name: 'Olahraga'},
                { name: 'Bola'},
                { name: 'Fisik'}
            ] 
        }))
    }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('It should show the correct amount of unselected tags', async () => {
    render(
        <MemoryRouter>
            <RekomendasiUKM getRecommended={() => null} />
        </MemoryRouter>
    );

    const tags = await waitFor(() => screen.getAllByRole("tags"));   
    
    expect(tags).toHaveLength(3);
});

test('It should be able to select the unselected tags', async () => {
    render(
        <MemoryRouter>
            <RekomendasiUKM getRecommended={() => null} />
        </MemoryRouter>
    );

    const tags = await waitFor(() => screen.getAllByRole("tags"));   
    
    fireEvent.click(tags[0]);

    const selectedTags = await waitFor(() => screen.getAllByRole("selectedTags"));   
    const newTags = await waitFor(() => screen.getAllByRole("tags"));   
    
    expect(selectedTags).toHaveLength(1);
    expect(newTags).toHaveLength(2);
});

test('It should be able to unselect the selected tags', async () => {
    render(
        <MemoryRouter>
            <RekomendasiUKM getRecommended={() => null} />
        </MemoryRouter>
    );

    const tags = await waitFor(() => screen.getAllByRole("tags"));   
    
    fireEvent.click(tags[0]);

    const unselectButton = await waitFor(() => screen.getAllByRole("unselectSelectedTag"));

    fireEvent.click(unselectButton[0]);

    const newTags = await waitFor(() => screen.getAllByRole("tags"));   
    const newSelectedTags = screen.queryAllByRole("selectedTags");   
    
    expect(newSelectedTags).toHaveLength(0);
    expect(newTags).toHaveLength(3);
});
